FROM node:14

COPY . /app

WORKDIR /app

RUN npm i

ENV PORT=3000

CMD ["npm", "start"]